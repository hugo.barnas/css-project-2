# css project 2 with css flex
Learn techno  css flexbox

## Techno used

- Visual studio code
- css flex

## Results

1. Mobile

![Capture_d_écran_2021-12-05_à_18.39.25](/uploads/29f6f86fb1579c7fa8327d0a161253b1/Capture_d_écran_2021-12-05_à_18.39.25.png)

2. Desktop

![Capture_d_écran_2021-12-05_à_18.39.41](/uploads/737f09e71a4cfc13ebd5e019eab503f6/Capture_d_écran_2021-12-05_à_18.39.41.png)


## Authors and acknowledgment
Exercice from the web site : [10 css projects in 10 days](https://dev.to/coderamrin/build-10-css-projects-project-2-57ec)

## License
For open source projects, say how it is licensed.
